CC=gcc
FLAGS=-lm
OBJETS=tp4.o lists.o mt.o

all: tp4

tp4: $(OBJETS)
	$(CC) -o $@ $(OBJETS) $(FLAGS)

tp4.o: tp4.c mt.h lists.h structs.h
	$(CC) -c $<

lists.o: lists.c structs.h
	$(CC) -c $<

mt.o: mt.c
	$(CC) -c $<