/*
Un en-tête appelé lists.h contenant toutes les définitions
des fonctions codées dans "lists.c"
*/

#include "structs.h"

#ifndef LISTS
#define LISTS

rabbit_t * initRabbit(bool isFemale);

rabbitList_t * initList();
void add(rabbit_t * rabbit, rabbitList_t * list);
element_t * removeElt(element_t * element, rabbitList_t * list);
int countList(rabbitList_t * list);
void printList(rabbitList_t * list, bool allRabbits);
void deleteList(rabbitList_t * list);

birthTab_t * initBirthTab();
void addMonth(birthTab_t * tab);
int nbLitters(rabbit_t * rabbit);

#endif