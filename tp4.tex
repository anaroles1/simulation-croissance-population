\documentclass[a4paper,12pt]{article}

\usepackage[utf8]{inputenc}  % codage des caractères
\usepackage[T1]{fontenc} % gestion d'accents
\usepackage{hyphsubst} % babel râle sans ça
\usepackage[french]{babel} % pour respecter les conventions françaises (listes à puces…) et couper les mots correctement
\usepackage{indentfirst} % pour indenter le 1er paragraphe
\usepackage[skip=1em, indent]{parskip} % pour sauter une ligne entre les paragraphes
\usepackage{listings} % pour insérer du code
\usepackage{graphicx} % pour inclure des images (PNG, JPG, PDF)
\usepackage{color}
\usepackage{multicol} % pour mettre le texte sur plusieurs colonnes
\usepackage{array} % plus de paramètres pour les tableaux


\lstset{language=C, frame=single, numbers=left, showstringspaces=false}

\hyphenation{removeElt}
\hyphenation{main}
\hyphenation{simul}

\title{TP4 : Simulation d'une croissance de population}  % {} laisser le champ vide
\author{Antoine \bsc{Aroles} \and Hugo \bsc{Triolet}} 
\date{}  % {} laisser le champ vide

\begin{document}

\maketitle

\tableofcontents

\pagebreak

\section{Présentation}
Le programme codé et qui engendre ce rapport résulte d'une volonté de simuler de manière stochastique l'évolution d'une population de lapins typés Garenne.

Pour cette espèce de lapins, nous n'avons pas codé de prédateurs, aussi la population de lapins ne sera peu ou pas régulée lors de nos simulations (la régulation sans prédateur est donc plus faible). Quelque soit le nombre de lapins de départ, les caractéristiques suivantes seront mises en œuvre :
\begin{itemize}
\item un lapin devient fertile entre 5 et 8 mois.
\item une lapine ne peut mettre bas qu'une fois par mois.
\item la gestation dure un mois.
\item une lapine a entre 4 et 8 portées par an.
\item une portée contient entre 3 et 6 lapereaux.
\item à l'année, la chance de survie d'un lapin adulte est de 60\% contre 35\% pour un jeune.
\item après 10 ans, un lapin voit ses chances de survie diminuer de 10\% par an jusqu'à 15 ans, où l'on considère qu'il meurt de vieillesse.
\end{itemize}


Le programme se structure en plusieurs modules et en-têtes : \texttt{tp4.c}, \texttt{lists.c}, \texttt{lists.h}, \texttt{mt.c}, \texttt{mt.h} et \texttt{structs.h}. Le générateur de nombres aléatoires que nous avons utilisé est le \texttt{Mersenne Twister} codé par M. Takuji Nishimura et M. Makoto Matsumoto car il possède une très grande période et est de ce fait très fiable.
Le programme est compilé via un \texttt{makefile} que nous avons écrit.

\begin{itemize}
\item \texttt{tp4.c} contient le \textsf{main} ainsi que la déclaration des fonctions appelées explicitement dans la fonction \textsf{simul} qui sera la fonction appelée dans le  \textsf{main}.
\item \texttt{lists.c} contient toutes les descriptions des fonctions permettant de manipuler les structures que nous avons codées pour ce programme. 
\item \texttt{lists.h} contient toutes les déclarations des fonctions de \texttt{lists.c}.
\item \texttt{mt.c} contient les fonctions du Mersenne Twister et elles seront déclarées dans \texttt{mt.h}.
\item \texttt{structs.h} contient toutes les descriptions et déclarations d'alias des structures utilisées dans le programme.
\end{itemize}
Dans un premier temps, il sera détaillé l'implémentation de notre programme puis enfin, les résultats obtenus.

\section{Implémentation}

\subsection{Vue générale}
Le programme est structuré par la fonction \textsf{simul} située dans le fichier \texttt{tp4.c}. Elle prend en entrée une liste de lapins représentant la population de ces derniers et un nombre de mois et simule l'évolution de la population de lapins sur la durée indiquée en modifiant la liste au fur et à mesure de son exécution avec un pas d'un mois. Le pas d'un mois permet une simulation plus précise et donc plus réaliste qu'un pas d'un an. Elle indique quel lapin doit subir quel traitement mais délègue ces traitements aux autres fonctions du fichier dont nous détaillerons le fonctionnement ci-dessous. Elle va, à chaque mois et pour chaque lapin : 
\begin{itemize}
\item vérifier s'il meurt, et le supprimer de la liste si c'est le cas
\item s'il est enceinte, le faire accoucher
\item si c'est une femelle fertile, la faire tomber enceinte avec une certaine probabilité
\item mettre son âge à jour
\end{itemize}

Nous allons maintenant expliquer comment ces traitements ont été implémentés et quels choix ont été faits.

\subsection{Structures}

Les données sont représentées dans le programme au moyen de différentes structures que nous allons présenter. Elles sont toutes déclarées dans le fichier \texttt{structs.h} et les fonctions permettant de les manipuler se situent dans le fichier \texttt{lists.c}

\subsubsection{rabbit\_t}
Un lapin, que nous considérons comme une entité possédant une "carte d'identité" (donc un ensemble d'attributs), sera modélisé par une structure \textsf{struct rabbit} de type \textsf{rabbit\_t}.
Notre structure représentant le lapin possède comme attributs :
\begin{itemize}
\item \texttt{isFemale}, booléen représentant le sexe du lapin (\textit{true} si femelle)
\item \texttt{isPregnant}, booléen représentant l'état de grossesse du lapin (false si mâle ou pas enceinte)
\item \texttt{isFertile}, booléen représentant le caractère fertile ou non du lapin
\item \texttt{aYearFertile}, booléen indiquant si le lapin est fertile depuis plus d'un an ou non
\item \texttt{age}, entier représentant son age en mois
\item \texttt{number}, entier ayant le rôle d'identifiant unique pour le lapin
\item \texttt{birthTab}, structure \textsf{struct birthTab} de type \textsf{birthTab\_t} contenant un entier faisant office de curseur et un tableau circulaire d'entiers. Cet attribut identifie les accouchements sur les 12 derniers mois
\end{itemize}

La fonction \textsc{initRabbit} permet de créer un lapereaux en prenant son sexe en paramètre. L'attribut \texttt{number} sert au débogage. Son unicité est assurée par le fait qu'il est déterminé par une variable globale (\texttt{rabbitNumber} déclarée dans \textsf{lists.c}) incrémentée à chaque initialisation de lapin.

\subsubsection{birthTab\_t}
Pour connaitre la probabilité pour une lapine de tomber enceinte il nous faut savoir combien elle a fait de grossesses au cours des 12 derniers mois. Nous avons donc décidé de stocker  l'information des grossesses de chaque lapine dans un tableau en-capsulé dans une structure \textsf{birthTab\_t}. Cette structure, en plus du tableau, contient un entier indiquant la case du tableau correspondant au mois courant. Un \texttt{birthTab} est initialisé via la fonction \textsf{initBirthTab}. La dernière case du tableau est initialisée à $-1$, ce qui permet de savoir facilement si un lapin est fertile depuis plus d'un an afin de mettre à jour l'attribut \texttt{aYearFertile}. La fonction \textsf{addMonth} qui ajoute un à l'entier indiquant la premier case (ou le remet à $0$ si la dernière case a été atteinte) et met la case à $0$ est appelée à la fin de chaque mois. Si la lapine tombe enceinte dans le mois, la case courante prendra la valeur $1$. La fonction \textsf{nb\_litters} permet alors de savoir combien de fois la lapine a été enceinte au cours des 12 derniers mois en comptant les cases du tableau à $1$.

\subsubsection{rabbitList\_t}
La population des lapins est représentée au moyen d'une liste doublement chainée. Comme à chaque accouchement de lapine il est nécessaire d'ajouter des lapins au début de la liste, une structure de liste chainée nous a paru adaptée de part la simplicité de cet ajout en début de liste, là où cela aurait été plus compliqué avec une structure basée sur un tableau sans connaissance préalable de la taille maximale de la population. Lorsqu'on supprime un élément d'une liste simplement chainée, il est nécessaire de la parcourir depuis le début afin de lier l'élément qui précède l'élément à supprimer à celui qui le suit. Cependant, nous parcourons déjà la liste dans la fonction \textsf{simul}, le fait d'utiliser une liste doublement chainée nous permet donc de supprimer un élément beaucoup plus rapidement grâce à une référence vers l'élément précédant.

Concrètement les éléments de la liste sont implémentés via des structures nommés \textsf{element\_t} contenant un pointeur vers un lapin, un pointeur vers l'élément précédant (qui vaudra \texttt{NULL} pour le premier élément) et un pointeur vers l'élément suivant (qui vaudra \texttt{NULL} pour le dernier élément). La liste elle même est une structure nommée \textsf{rabbitList\_t} contenant simplement un pointeur vers le premier élément.

Une liste est initialisée via la fonction \textsf{initList} qui retourne un pointeur vers une liste vide (i.e le pointeur vers le premier élément vaut \texttt{NULL}). La fonction \textsf{add} permet d'ajouter un lapin en début de liste. Ceci nous permet de traiter les lapins nés au cours d'un un mois seulement à partir du mois suivant, ce qui est le comportement que nous souhaitions. La fonction \textsf{removeElt} permet de supprimer proprement un élément (en désallouant toute la mémoire allouée dans les structures imbriqués dans l'élément). Pour les besoins de la fonction \textsf{simul}, elle revoie un pointeur vers l'élément qui suivait l'élément supprimé .La fonction \textsf{deleteList} permet de supprimer proprement la liste. Le contenu de la liste peut aussi être affiché via la fonction \textsf{printList} et la fonction \textsf{countList} permet d'indiquer la taille de la liste (donc le nombre de lapins qu'elle contient).

\subsection{Fonctionnement}

Nous allons maintenant détailler les fonctions du fichier \texttt{tp4.c}, appelées au cours de l'exécution de \textsf{simul}.

\subsubsection{mort des lapins}
La première chose que l'on fait dans \textsf{simul} est déterminer si le lapin meurt ou non. Les taux de survie annuels implémentés sont ceux indiqués dans l'énoncé. Cette implémentation est définie dans la fonction \textsf{dies} qui renvoie \texttt{true} si le lapin meurt et \texttt{false} sinon. Dans le cas où le lapin meurt, il est supprimé de la liste et on peut facilement passer au lapin suivant grâce au fait que l'élément contenant ce dernier soit renvoyé par \textsf{removeElt}. 

Comme notre simulation ne se fait pas avec un pas d'un an mais avec un pas d'un mois, il a fallu calculer les taux de survie mensuels à partir des taux de survie annuels. Pour les taux fixes (avant 10 ans), il suffit de prendre le taux annuel à la puissance $\frac{1}{12}$. Nous avons calculé ces grandeurs et les avons écrites dans le programme avec une précision de 7 chiffres après la virgule largement suffisants pour notre utilisation. Au delà de 10 ans en revanche, le taux de survie est censé diminuer de 10\% par an. Plutôt que de faire des marches tous les ans, nous avons naturellement choisit de faire décroitre ce taux tous les mois. À partir de 120 mois, la fonction qui à un age en mois associe un taux de survie annuel est donc une fonction affine valant $0.6$ en $120$ et $0$ en $180$. On passe encore une fois au taux de survie mensuel en mettant l'image de cette fonction à la puissance $\frac{1}{12}$.

Pour être sûr de ne pas avoir fait d'erreur sur cette fonction, nous avons créé la fonction \textsf{verifyDies} qui implémente directement les taux de survie annuels afin de les comparer avec nos résultats sans reproduction, et les résultats se sont avéré cohérents.
\subsubsection{atteinte de la maturité sexuelle}
L'age de maturité sexuelle est déterminé dans la fonction \textsf{becomeFertile} selon une loi uniforme entre 5 et 8 mois. Le lapin est alors en capacité de se reproduire. Si c'est une femelle, son \texttt{birthTab} est initialisé à ce moment là.

\subsubsection{reproduction}
Après avoir vérifié que la lapine était bien fertile dans \textsf{simul}, et éventuellement l'avoir fait accoucher au préalable si déjà enceinte durant le mois, la fertilisation ou non de celle-ci devient la question qui se pose.\\
La fertilisation d'une lapine est déterminée dans la fonction \textsf{becomePregnant}. \\
La fonction prend en argument un élément de la liste (un lapin en somme), et assigne à une variable locale la structure du lapin et à une autre variable locale le \textsf{birthTab}. On tire aussi un nombre aléatoire entre 0 et 1 et on calcule depuis le \textsf{birthTab} son nombre de portées sur les 12 derniers mois. Ensuite, deux choix s'offrent au lapin.\\
Si il n'est pas fertile depuis au moins un an et qu'il a eu moins de 4 portées, alors on passe son nombres de portées artificiellement à 4 pour pouvoir utiliser la partie d'après pour les lapins fertiles depuis un an.\\
Si le lapin est fertile depuis au moins un an, deux cas :
\begin{itemize}
\item si le lapin a eu moins de 4 portées sur les 12 derniers mois, alors il est garantit de tomber enceinte (ceci afin de garantir d'avoir au minimum 4 portée par an)
\item si le lapin a eu entre 4 et 7 portées sur les 12 derniers mois alors on compare le nombre aléatoire tiré avec une valeur comprise entre 0 et 1 que l'on a déterminé via une loi de probabilité. 
\end{itemize}
En effet, nous savons qu'il est plus probable d'avoir entre 5 et 7 portées par an pour un lapin donc 4 ou 8 portées sont des cas plus rares que 5, 6 et 7. Pour que cela soit assez simple et ressemble à une répartition normale, nous avons choisit d'attribuer les probabilités suivantes : $\frac{1}{8}$ de chances pour 4 et 8 portées, $\frac{3}{16}$ pour 5 et 7 portées et $\frac{3}{8}$ pour 6 portées.
\begin{center}
\includegraphics{portee_annee.png}
\end{center}
Ainsi, si le lapin tombe enceinte, son attribut \textsf{isPregnant} est passé à \textit{true} et on passe la case du mois courant dans \textsf{birthTab} à 1 (pour signifier le nombre de portées dans notre programme, et pouvant servir plus tard si nous décidons  d'améliorer le programme et de compter une probabilité de fausse couche).

\subsubsection{accouchement}
Lors de l'accouchement des lapines, il faut déterminer le nombre de lapereaux obtenus. Conformément à ce qui est indiqué dans l'énoncé, ce nombre est choisi dans la fonction \textsf{giveBirth} selon une loi uniforme entre 3 et 6. Le sexe du lapereau est alors déterminé avec une probabilité $1/2$. Chaque lapereau est ajouté en début de liste pour être traité dès le mois suivant.

\section{Résultats}

\end{document}