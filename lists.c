/*
Un module C du programme principal "tp4.c" nommé "lists.c"
qui contient toutes les descriptions des fonctions permettant
de manipuler toutes les structures présentes dans "structs.h"  
*/

#include <stdio.h>
#include <stdlib.h>
#include "structs.h"

/*=======================Fonctions sur les rabbit_t=========================*/

int rabbitNumber = 0; //pour identifier les lapins via un numero unique

/*--------------------------------------------------------------------------*/
/*initRabbit                créé un lapin (struct rabbit_t)					*/
/*                                                                          */
/*En entrée : un booléen pour déterminer le sexe      						*/
/*                                                                          */
/*En sortie : un nouveau lapin est renvoyé                                  */
/*--------------------------------------------------------------------------*/

rabbit_t * initRabbit(bool isFemale) 
{

	rabbit_t * rabbit = malloc(sizeof(rabbit));
	
	rabbit->isFemale 		= isFemale;
	rabbit->isPregnant 		= false;
	rabbit->isFertile 		= false;
	rabbit->aYearFertile 	= false;
	rabbit->age 			= 0;
	rabbit->birthTab 		= NULL;
	rabbit->number 			= rabbitNumber++;

	return rabbit;
}

/*=====================Fonctions sur les rabbitList_t=======================*/

/*--------------------------------------------------------------------------*/
/*initList                créé une liste de lapins 							*/
/*                                                                          */
/*En entrée : rien                                  						*/
/*                                                                          */
/*En sortie : la liste créée                                                */
/*--------------------------------------------------------------------------*/

rabbitList_t * initList()
{

	rabbitList_t * list = malloc(sizeof(*list));

	list->first = NULL;

	return list;
}

/*--------------------------------------------------------------------------*/
/*add               ajoute un lapin au début d'une liste			 		*/
/*                                                                          */
/*En entrée : 	-rabbit, le lapin à ajouter                                 */
/*				-list, la liste dans laquelle ajouter le lapin 				*/
/*                                                                          */
/*En sortie : rien                                                          */
/*--------------------------------------------------------------------------*/

void add(rabbit_t * rabbit, rabbitList_t * list)
{

	element_t * newElement = malloc(sizeof(element_t));

	newElement->rabbit 	= rabbit;
	newElement->prev 	= NULL;
	newElement->next 	= list->first;

	if(list->first != NULL) 
	{
		list->first->prev = newElement;
	}

	list->first = newElement;
}

/*--------------------------------------------------------------------------*/
/*removeElt           	supprime un élement d'une liste de lapins           */
/*                                                                          */
/*En entrée : l'element à supprimmer et la liste à laquelle il appartient   */
/*                                                                          */
/*En sortie : un pointeur vers l'élement qui suivait l'élement supprimé     */
/*--------------------------------------------------------------------------*/

element_t * removeElt(element_t * element, rabbitList_t * list)
{

	element_t * nextElement = malloc(sizeof(element_t));

	// si l'élement est seul dans la liste
	if(list->first == element && element->next == NULL) 
	{
		nextElement = NULL;
		list->first = NULL;
	}
	// si l'élement est premier de la liste
	else if(list->first == element) 
	{
		element->next->prev = NULL;
		list->first = element->next;
		nextElement = list->first;
	}
	// si l'élement est dernier de la liste
	else if(element->next == NULL) 
	{
		element->prev->next = NULL;
		nextElement = NULL;
	}
	// cas général
	else 
	{
		element->next->prev = element->prev;
		element->prev->next = element->next;
		nextElement = element->next;
	}

	//suppression du birthTab
	if(element->rabbit->birthTab != NULL) 
	{
		free(element->rabbit->birthTab->tab);
		free(element->rabbit->birthTab);
	}

	free(element->rabbit);
	free(element);

	return nextElement;
}

/*--------------------------------------------------------------------------*/
/*countList          compte le nombre de lapins dans une liste           	*/
/*                                                                          */
/*En entrée : la liste                             							*/
/*                                                                          */
/*En sortie : le nombre d'élements     										*/
/*--------------------------------------------------------------------------*/

int countList(rabbitList_t * list) 
{
	element_t * element = list->first;

	int elementsNumber = 0;

	while(element != NULL) 
	{
		elementsNumber++;
		element = element->next;
	}

	return elementsNumber;
}

/*--------------------------------------------------------------------------*/
/*printList           	affiche une liste de lapins 				        */
/*                                                                          */
/*En entrée : 	-un pointeur sur la liste à afficher                       	*/
/*				-un booléen indiquant si on veut afficher tous les lapins 	*/
/*				ou seulement les femelles 									*/
/*                                                                          */
/*En sortie : rien     														*/
/*--------------------------------------------------------------------------*/

void printList(rabbitList_t * list, bool allRabbits) 
{

	element_t * currentElement = list->first;
	rabbit_t rabbit;

	while(currentElement != NULL) 
	{
		rabbit = *(currentElement->rabbit);

		if(allRabbits || rabbit.isFemale)
		{
			printf("{rabbit ; number: %d, isFemale: %d, isPregnant: %d, isFertile: %d, aYearFertile: %d, age: %d}\n", \
				rabbit.number, rabbit.isFemale, rabbit.isPregnant, rabbit.isFertile, rabbit.aYearFertile, rabbit.age);

			// afichage du birthTab
			if(rabbit.birthTab != NULL) 
			{
				printf("\tbirthTab : %d", rabbit.birthTab->tab[0]);
				for (int i = 1 ; i < 12 ; i++) 
				{
					printf(", %d", rabbit.birthTab->tab[i]);
				}

				printf(" ; first element index : %d\n", rabbit.birthTab->first);
			}
		}

		currentElement = currentElement->next;

	}
	printf("\n");
}

/*--------------------------------------------------------------------------*/
/*deleteList                supprime une liste de lapins 	                */
/*                                                                          */
/*En entrée : la liste à supprimer                                          */
/*                                                                          */
/*En sortie : rien                                                          */
/*--------------------------------------------------------------------------*/

void deleteList(rabbitList_t * list)
{

	if(list->first != NULL)
	{
		element_t * currentElement;
		element_t * nextElement;

		currentElement 	= list->first;

		while(currentElement->next != NULL)
		{
			nextElement = currentElement->next;

			//suppression birthTab
			if(currentElement->rabbit->birthTab != NULL) 
			{
				free(currentElement->rabbit->birthTab->tab);
				free(currentElement->rabbit->birthTab);
			}

			free(currentElement->rabbit);
			free(currentElement);

			currentElement = nextElement;
		}

		free(currentElement->rabbit);
		free(currentElement);
	}

	free(list);
}

/*======================Fonctions sur les birthTab_t========================*/

/*--------------------------------------------------------------------------*/
/*initBirthTab             initialise une structure birthTab 				*/
/*                                                                          */
/*En entrée : rien                                  						*/
/*                                                                          */
/*En sortie : la strucutre créée                                            */
/*--------------------------------------------------------------------------*/

birthTab_t * initBirthTab() 
{

	birthTab_t * tab = malloc(sizeof(birthTab_t));

	tab->first 	= 0;
	tab->tab 	= malloc(12 * sizeof(int));

	//on initialise la dernière case à -1
	tab->tab[11] = -1;

	return tab;
}


/*--------------------------------------------------------------------------*/
/*addMonth           passe une structure birthTab au mois suivant 			*/
/*                                                                          */
/*En entrée : la structure à modifier                                  		*/
/*                                                                          */
/*En sortie : rien                                            				*/
/*--------------------------------------------------------------------------*/

void addMonth(birthTab_t * tab) 
{
	tab->first ++;
	tab->first %= 12;
	tab->tab[tab->first] = 0;
}

/*--------------------------------------------------------------------------*/
/*nbLitters       compte le nombre de portées d'un lapin au cours  			*/
/*								des 12 derniers mois 						*/
/*                                                                          */
/*En entrée : le lapin                                   					*/
/*                                                                          */
/*En sortie : le nombre de portées                                          */
/*--------------------------------------------------------------------------*/

int nbLitters(rabbit_t * rabbit) 
{
	int nbLitters = 0;

	for (int i = 0 ; i < 12 ; i++) 
	{
		if(rabbit->birthTab->tab[i] == 1) 
		{
			nbLitters += 1;
		}
	}

	return nbLitters;
}