/*
En-tête nommé structs.h contenant la description de chacune
des structures utilisées dans le programme principal tp4.c
*/

#include <stdbool.h>

#ifndef STRUCTS
#define STRUCTS

typedef struct rabbit rabbit_t;
typedef struct element element_t;
typedef struct doubleLinkedRabbitList rabbitList_t;
typedef struct birthTab birthTab_t;

//permet de stocker les accouchement des lapins sur les 12 derniers mois
struct birthTab {
	int first;
	int * tab;
};

struct rabbit{
	bool isFemale;
	bool isPregnant;
	bool isFertile;
	bool aYearFertile; 		// true si le lapin est fertile depuis plus d'un an
	int  age; 				// age en mois
	int number; 			// pour le débogage
	birthTab_t * birthTab;
};

struct element{
	rabbit_t 	* rabbit;
	element_t 	* prev;			// null pour le premier element
	element_t 	* next;			// null pour le dernier element
};

struct doubleLinkedRabbitList{
	element_t * first;
};

#endif