/*
	tp4.c

	Fichier principal du programme de simulation de l'évolution
	d'une population de lapins. Il contient la fonction main ansi
	que la fonction simul qui est au centre du programme. Outre
	les fonctions initGene et fibo, toutes les fonctions présentes
	avant simul sont appelés uniquement par cette dernière. 
*/

#include <stdio.h>
#include <stdlib.h>
#include <stdbool.h>
#include <math.h>
#include "mt.h"
#include "lists.h"
#include "structs.h"


/*--------------------------------------------------------------------------*/
/*initGene                Initialisation du générateur                      */
/*                                                                          */
/*En entrée : rien                                                          */
/*                                                                          */
/*En sortie : rien                                                          */
/*--------------------------------------------------------------------------*/

void initGene()
{
    unsigned long init[4]={0x123, 0x234, 0x345, 0x456}, length=4;
    init_by_array(init, length);
}

/*--------------------------------------------------------------------------*/
/*fibo                Retourne le nombre de lapins au n-ième mois 			*/
/*						dans le modèle défini à la question 1               */
/*                                                                          */
/*En entrée : n, le numéro du mois (en commençant à 0)                      */
/*                                                                          */
/*En sortie : le nombre de couples au mois n                                */
/*--------------------------------------------------------------------------*/

long fibo(int n)
{
	long * tabValues = malloc((n+1) * sizeof(long));
	tabValues[0] = 1;
	tabValues[1] = 1;

	for(int i = 2 ; i <= n ; i++){
		tabValues[i] = tabValues[i-1] + tabValues[i-2];
	}

	long result = tabValues[n];
	free(tabValues);
	return result;
}

/*--------------------------------------------------------------------------*/
/*                                                                			*/
/*dies : revoie un booléen comme information sur la mort du     			*/
/*            lapin                                                 		*/
/*                                                                			*/
/*Entrée : un rabbit_t, symbolisant un individu de la population            */
/*                                                                			*/
/*Sortie : booléen quant la mort ou non de l'individu           			*/
/*                                                              			*/
/*--------------------------------------------------------------------------*/  

bool dies(rabbit_t * rabbit)
{
	//mise en oeuvre de la perte de chances de survie passé 10 ans
    if(rabbit->age >= 120)
    {
        return genrand_real1() <= 1 - (pow(1.8 - 0.01*rabbit->age, 1./12.));
    }
    else if(rabbit->isFertile)
    {
    	// 60% de chances de survie annuelle
        return genrand_real1() <= 0.0416755;
    }
    else
    {	
    	// 35% de chances de survie annuelle
        return genrand_real1() <= 0.0837675;
    }
}

/*--------------------------------------------------------------------------*/
/*becomeFertile       Détermine si un lapin devient fertile selon son age 	*/
/*						d'après une loi uniforme entre 5 et 8 mois          */
/*                                                                          */
/*En entrée : un pointeur sur l'élement contenant le lapin                  */
/*                                                                          */
/*En sortie : rien, la propriété isFerile du lapin est directement modifiée */
/*--------------------------------------------------------------------------*/

void becomeFertile(element_t * element)
{

	rabbit_t * rabbit = element->rabbit;

	//pas besoin de faire un tirage si le lapin n'a pas 5 mois
	if(rabbit->age < 5)
	{
		return;
	}

	//loi uniforme entre 5 et 8 mois
	else if(genrand_real1() <= 0.25*(rabbit->age-4))
	{
		rabbit->isFertile = true;

		if(rabbit->isFemale)
		{
			rabbit->birthTab = initBirthTab();
		}
		
		return;
	}
}

/*--------------------------------------------------------------------------*/
/*giveBirth          simuation de l'accouchement d'une lapine               */
/*                                                                          */
/*En entrée : l'element contenant la lapine et la liste de lapins           */
/*                                                                          */
/*En sortie : rien, les nouveaux-nés sont directement ajoutés à la liste    */
/*--------------------------------------------------------------------------*/

void giveBirth(element_t * element, rabbitList_t * list)
{	
	//le nombre de petits est déterminé selon une loi uniforme entre 3 et 6
	int kittensNumber = (int) (4. * genrand_real2() + 3.);
	
	rabbit_t * rabbit;
	for (int i = 0 ; i < kittensNumber ; i++)
	{
		//determination du sexe du lapereau
		if(genrand_real1() <= 0.5)
		{
			rabbit = initRabbit(false);
		}
		else
		{
			rabbit = initRabbit(true);
		}
		add(rabbit, list);
	}
}

/*--------------------------------------------------------------------------*/
/*                                                        					*/
/* becomePregnant : simulation de la fertilisation d'une lapine             */
/*                                                        					*/
/* Entrée : l'élément contenant la lapine                					*/
/*                                                        					*/
/* Sortie : rien, modification ou non du statut du lapin 					*/
/*            quant à sa fertilisation                    					*/
/*                                                        					*/
/*--------------------------------------------------------------------------*/

void becomePregnant(element_t * element)
{
    rabbit_t * rabbit = element->rabbit;
    birthTab_t * birthTab = rabbit->birthTab;

    int nb_litter = nbLitters(rabbit);
    double proba = genrand_real1();

    //si la lapine est fertile depuis moins d'un an, elle a la même \
    proba de tomber enceinte que si elle avait accouché 4 fois
    if(!rabbit->aYearFertile && nb_litter < 4)
    {
    	nb_litter = 4;
    }

    if(nb_litter < 4)
    {
        rabbit->isPregnant = true;
        birthTab->tab[birthTab->first] = 1;
    }

    else
    {
        switch(nb_litter)
        {
            //plus de chance d'avoir entre 5 et 7 portées donc \
            plus de chance d'en avoir plus de 4 et moins d'en avoir plus de 7
            case 4:
                if(proba <= 0.875)
                {
                    rabbit->isPregnant = true;
                    birthTab->tab[birthTab->first] = 1;
                }
                break;

            case 5:
                if(proba <= 0.6875)
                {
                    rabbit->isPregnant = true;
                    birthTab->tab[birthTab->first] = 1;
                }
                break;

            case 6:
                if(proba <= 0.3125)
                {
                    rabbit->isPregnant = true;
                    birthTab->tab[birthTab->first] = 1;
                }
                break;

            case 7:
                if(proba <= 0.125)
                {
                    rabbit->isPregnant = true;
                    birthTab->tab[birthTab->first] = 1;
                }
                break;

            default :
                break;
        }
    }
}

/*--------------------------------------------------------------------------*/
/*simul                renvoie un tableau contenant les lapins				*/
/*							vivants à chaque mois			          		*/
/*                                                                          */
/*En entrée : -*rabbitList, un pointeur vers une liste des lapins vivants 	*/
/*				qui est actualisée au fil de l'exécution du programme. 		*/
/*			  -duration, la durée de la simulation 							*/
/*                                                                          */
/*En sortie : rien, rabbitsList est modifiée par effet de bord             	*/
/*--------------------------------------------------------------------------*/

void simul(rabbitList_t *rabbitList, int duration)
{
	for(int month = 0 ; month < duration ; month++)
	{
		element_t 	* currentElement = rabbitList->first;
		rabbit_t 	* rabbit;
		
		while(currentElement != NULL)
		{
			rabbit = currentElement->rabbit;
			
			//si le lapin meurt
			if(dies(rabbit))
			{
				//currentElement devient l'element suivant
				currentElement = removeElt(currentElement, rabbitList);

				//on passe à l'itération suivante
				continue;
			}
			
			if(!rabbit->isFertile)
			{
				becomeFertile(currentElement);
			}
			
			if(rabbit->isFemale)
			{
				if(rabbit->isPregnant)
				{
					//détermine le nombre d'enfants et les ajoute à la liste
					giveBirth(currentElement, rabbitList);
				}
				
				if(rabbit->isFertile)
				{
					becomePregnant(currentElement);
					addMonth(rabbit->birthTab);
					
					if(!rabbit->aYearFertile)
					{
						//on vérifie si la lapine est fertile depuis plus d'un an\
						en regardant si la derniere case de son birthTab a été modifié
						if(rabbit->birthTab->tab[11] != -1)
						{
							rabbit->aYearFertile = true;
						}
					}
				}	
			}

			rabbit->age += 1;
			
			currentElement = currentElement->next;
		}
	}
}

/*--------------------------------------------------------------------------*/
/*expectedRabbitsLeft 	calcul le nombre attendu de lapins vivants 			*/
/*					d'une population exclusivement masculine en fonction 	*/
/*								des taux de survie annuels            		*/
/*                                                                          */
/*En entrée : le nombre initial de lapins           						*/
/*                                                                          */
/*En sortie : un tableau contenant le nombre de lapins attendus après 		*/
/*				chaque année 												*/
/*--------------------------------------------------------------------------*/

float * expectedRabbitsLeft(int initialNumber)
{
	float * tab = malloc(15 * sizeof(float));

	//taux initial obtenu experimentalement
	tab[0] = 0.434 * initialNumber;

	int i;
	float survivalRate = 0.6;
	for(i = 1 ; i < 10 ; i++)
	{
		tab[i] = survivalRate * tab[i-1];
	}

	for(i = 10 ; i < 15 ; i++)
	{
		survivalRate -= 0.1;
		tab[i] = survivalRate * tab[i-1];
	}

	return tab;
}

/*--------------------------------------------------------------------------*/
/*verifyDies          compare les taux de déces de la fonction dies 		*/
/*						à ceux calculés par expectedRabbitsLeft 			*/
/*                                                                          */
/*En entrée : rien           												*/
/*                                                                          */
/*En sortie : rien    														*/
/*--------------------------------------------------------------------------*/

void verifyDies()
{
	int rabbitsNumber = 10000000;
	rabbit_t * rabbit;
	rabbitList_t * list = initList();

	float * expectedTab = expectedRabbitsLeft(rabbitsNumber);

	for(int i = 0 ; i < rabbitsNumber ; i++)
	{
		rabbit = initRabbit(false);
		add(rabbit, list);
	}

	int currentRabbitsNumber;
	for(int year = 1 ; year <= 15 ; year++)
	{
		simul(list, 12);
		currentRabbitsNumber = countList(list);
		printf("Taux de lapins apres %2d ans : %6.3f%c ; attendu : %6.3f%c ; difference : %f%c\n", \
			year, (currentRabbitsNumber/(float) rabbitsNumber) * 100, '%', \
			(expectedTab[year-1]/rabbitsNumber) * 100, '%', \
			(fabsf(currentRabbitsNumber - expectedTab[year-1])/currentRabbitsNumber) * 100, '%');
	}

	free(expectedTab);
	free(list);
}

int main(int argc, char* argv[])
{
	initGene();
	
	rabbitList_t * list = initList();

	rabbit_t * rabbit;
	
	for(int i = 0 ; i < 2 ; i++) {
		rabbit = initRabbit(true);
		add(rabbit, list);
		rabbit = initRabbit(false);
		add(rabbit, list);
	}

	
	for(int year = 1 ; year <= 7 ; year++) {
		simul(list, 12);
		printf("population après %d ans : %d lapins\n", year, countList(list));
		printf("fibo la meme annee : %ld\n\n", fibo(year*12));
	}

	deleteList(list);

	return 0;
}